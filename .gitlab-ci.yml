stages:
    - test
    - build
    - build-docs
    - deploy
    - zenodo

## Template for all OpenSCAD jobs to ensure they run the same
.scad-job:
    image: ubuntu:20.04

    variables:
      # prevents config waiting for user input, e.g. for tzdata
      DEBIAN_FRONTEND: noninteractive
      DEBCONF_NONINTERACTIVE_SEEN: "true"

    # Install everything needed for all OpenSCAD jobs so they run in a consistent environment
    before_script:
      - apt-get update -qq
      - apt-get -o DPkg::Options::=--force-confdef -y -qq install software-properties-common dirmngr apt-transport-https lsb-release ca-certificates python3-pip git xvfb imagemagick unzip inkscape wget
      - wget https://build.openflexure.org/assets/openscad_2021.01-1~20.04_amd64.deb -O /tmp/openscad_2021.01-1~20.04_amd64.deb
      - apt -o DPkg::Options::=--force-confdef -y -qq install /tmp/openscad_2021.01-1~20.04_amd64.deb
      - pip3 install -r requirements.txt

# Unit testing of the scad library functions
scad-lib-tests:
    extends: .scad-job
    stage: test

    script:
      - openscad/libs/test/test_dict.py

    only:
      - tags
      - merge_requests
      - web

# Code quality runs SCA2D to do static analysis of the .scad code 
code_quality:
    stage: test
    image: python:3.7
    allow_failure: true
    script:
      - pip install sca2d
      - sca2d --colour --gitlab-report openscad/
    artifacts:
        reports:
            codequality:
              - gl-code-quality-report.json
        expire_in: 1 week
    # Note this runs more often than others, this is to make sure that the result
    # both the head and base of merge requests have code quality reports to allow the
    # MR quality changes to display
    only:
      - tags
      - merge_requests
      - web
      - branches

# Build STL files
build:
    extends: .scad-job
    stage: build

    script:
      - git clone --depth=1 https://gitlab.com/openflexure/openflexure-microscope-extra
      # Some files seem to appear changed even though they shouldn't be which
      # messes up our auto version-string embeddeding, so we do a hard git reset
      # see https://gitlab.com/gitlab-org/gitlab/-/issues/325299
      - git reset --hard
      # Build STL files with OpenSCAD
      - mkdir -p /root/.local/share
      - ./build.py --generate-stl-options-json --include-extra-files --force-clean

    artifacts:
      expire_in: 1 week
      name: "${CI_PROJECT_NAME}-${CI_JOB_NAME}-${CI_COMMIT_REF_NAME}-${CI_COMMIT_SHORT_SHA}"
      paths:
        - docs/models/*.stl
        - docs/models/*.json

    only:
      - tags
      - merge_requests
      - web

# render assemblies - uses xvfb as a virtual x-server
render:
    extends: .scad-job
    stage: build

    script:
      - xvfb-run --auto-servernum --server-args "-screen 0 1024x768x24" ./render.py

    artifacts:
      expire_in: 1 week
      name: "${CI_PROJECT_NAME}-${CI_JOB_NAME}-${CI_COMMIT_REF_NAME}-${CI_COMMIT_SHORT_SHA}-renders"
      paths:
        - docs/renders/*.png

    only:
      - tags
      - merge_requests
      - web


# Build docs- this is at a later stage so it has access to the models
build-docs:
    stage: build-docs
    dependencies:
      - build
      - render
    image: "python:3.7"

    before_script:
      - python --version
      - pip install gitbuilding

    script:
      - cd docs
      - gitbuilding build-html
      - cd ..
      - mv docs/_site builds

    artifacts:
      expire_in: 1 week
      name: "${CI_PROJECT_NAME}-${CI_JOB_NAME}-${CI_COMMIT_REF_NAME}-${CI_COMMIT_SHORT_SHA}-docs"
      paths:
        - builds/

    only:
      - tags
      - merge_requests
      - web


# Deploy to builds.openflexure.org
deploy:
  stage: deploy
  dependencies:
    - build
    - render
    - build-docs
  image: ubuntu:latest

  before_script:
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
    - eval $(ssh-agent -s)
    - ssh-add <(echo "$SSH_PRIVATE_KEY_BATH_OPENFLEXURE_BASE64" | base64 --decode)
    - mkdir -p ~/.ssh
    - echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config

  script:
    # Install rsync if not already installed
    - 'which rsync || ( apt-get update -y && apt-get install rsync -y )'

    # Upload the builds folder to openflexure-microscope builds
    - rsync -hrvz -e ssh builds/ ci-user@openflexure.bath.ac.uk:/var/www/build/openflexure-microscope/${CI_COMMIT_REF_NAME} --delete

    # Zip the builds folder and upload it to the openflexure-microscope build root
    - 'which zip || ( apt-get update -y && apt-get install zip -y )'
    - (cd builds && zip -r "../${CI_PROJECT_NAME}-${CI_COMMIT_REF_NAME}.zip" .)
    - rsync -hrvz -e ssh "${CI_PROJECT_NAME}-${CI_COMMIT_REF_NAME}.zip" ci-user@openflexure.bath.ac.uk:/var/www/build/openflexure-microscope/

    # Run update-latest.py on the build server
    - ssh -t ci-user@openflexure.bath.ac.uk "/var/www/build/update-latest.py"

  artifacts:
    expire_in: 1 week
    name: "${CI_PROJECT_NAME}-${CI_COMMIT_REF_NAME}.zip"
    paths:
      - "${CI_PROJECT_NAME}-${CI_COMMIT_REF_NAME}.zip"

  only:
    - tags
    - web


zenodo:
  stage: zenodo
  image: ubuntu:18.04
  dependencies:
    - deploy
  before_script:
    - apt-get update -qq
    - apt-get -y -qq install git python3-pip
    - pip3 install -r scripts/zenodo/requirements.txt
  variables:
    ZENODO_USE_SANDBOX: "false"
  script:
    - mv "${CI_PROJECT_NAME}-${CI_COMMIT_REF_NAME}.zip" "${CI_PROJECT_NAME}-${CI_COMMIT_REF_NAME}-outputs.zip"
    - git archive "${CI_COMMIT_REF_NAME}" --format=zip --output="${CI_PROJECT_NAME}-${CI_COMMIT_REF_NAME}-source.zip"
    - python3 scripts/zenodo/upload_to_zenodo.py "${CI_PROJECT_NAME}-${CI_COMMIT_REF_NAME}-source.zip" "${CI_PROJECT_NAME}-${CI_COMMIT_REF_NAME}-outputs.zip"
  artifacts:
    # this is only a small html link, let's just keep it forever
    # gitlab doesn't understand "expire_in: never" yet though
    expire_in: 100000 years
    name: zenodo-${CI_PROJECT_NAME}-${CI_COMMIT_REF_NAME}-link.html
    paths:
      - zenodo-link.html
  only:
    - tags
    - web
